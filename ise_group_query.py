#!/usr/bin/env python
# Import libraries
import csv
import json
import requests
from getpass import getpass
# suppress certificate warning
from requests.packages.urllib3.exceptions import InsecureRequestWarning
requests.packages.urllib3.disable_warnings(InsecureRequestWarning)
#
def main():
    ise_is_reachable =False
    pan_hostname =""
    ise_ersadmin_pass =""
    ise_ersadmin =""
    isempty =  True
    print("\n")
    print("*** Enter ISE details or type bye to exit *** \n")
    print()
    while not ise_is_reachable: 
        #ISE PAN
        print()
        while not pan_hostname:       
            pan_hostname = input("Enter the ISE PAN Hostname: ")
            if pan_hostname.lower() =="bye" :
                return
        #ISE ERS username
        while not ise_ersadmin:       
            ise_ersadmin = input("Enter the ISE ERS Username: ")
            if ise_ersadmin.lower() =="bye" :
                return
        #ISE ERS password
        while not ise_ersadmin_pass:
            ise_ersadmin_pass = getpass("Enter the ISE ERS Password: ")
            if ise_ersadmin_pass.lower() =="bye" :
                return
        ise_root_cert = False
        print()
        ise_headers = {
            'Accept': "application/json",
            'content-type': "application/json",
            }
        #get a list of all groups
        allGroups=[]
        ise_url = 'https://{}:{}@{}:9060/ers/config/endpointgroup?size=100&page=1'.format(ise_ersadmin, ise_ersadmin_pass, pan_hostname)
        try:
            ise_response = requests.request('GET', ise_url ,verify=ise_root_cert, headers=ise_headers)  
        except requests.ConnectionError:
            print('!!! ISE PAN Node is not reachable, please try again !!! ') 
            pan_hostname =""
            continue
        if ise_response.status_code == 200:
            ise_is_reachable =True
            groups = ise_response.json()['SearchResult']['resources']
            for i in range(len(groups)):
                allGroups.append({'group_id':groups[i]['id'],
                'group_name':groups[i]['name']})
        elif ise_response.status_code == 401:
            print('!!! Invalid Username or Password, please try again !!!')
            ise_ersadmin =""
            ise_ersadmin_pass =""
            continue
    print('Please wait...\n')
    # Get the number of endpoints
    groupSummary=[]
    for group in allGroups:
        ise_url = 'https://{}:{}@{}:9060/ers/config/endpoint?filter=groupId.EQ.{}'.format(ise_ersadmin, ise_ersadmin_pass, pan_hostname, group['group_id'])
        try:
            ise_response = requests.request('GET', ise_url ,verify=ise_root_cert, headers=ise_headers)
        except:
            print('something went wrong when quering the endpoints in the {} group'.format(group['group_name']))
        if ise_response.status_code == 200:
            groupSummary.append({'group_name':group['group_name'],
                        'no_of_endpoints':ise_response.json()['SearchResult']['total']})
        else:
            print('something went wrong when quering the endpoints in the {} group'.format(group['group_name']))

    # Export to a CSV file
    with open('ise_groups.csv','w') as csvfile:
        csv_writer = csv.DictWriter(csvfile, fieldnames= list(groupSummary[0].keys()), delimiter=',', lineterminator='\n')
        csv_writer.writeheader() 
        for line in groupSummary:
            csv_writer.writerow(line)
    print("ISE groups exported to ise_group.csv!")  

if __name__ == "__main__":
    main()        



